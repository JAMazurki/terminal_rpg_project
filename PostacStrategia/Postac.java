package Lab07.PostacStrategia;

import Lab07.BronStrategia.Bron;

public abstract class Postac implements Eksploruj {
    String imie;
    Bron bron;
    String rodzaj;

    @Override
    public void eksploruj() {
        System.out.println("Czerpie radosc z tego, ze wedruje i zapominam o bozym swiecie...");
    }

    public String getImie() {
        return imie;
    }

    public void setImie(String imie) {
        this.imie = imie;
    }

    public Bron getBron() {
        return bron;
    }

    public void setBron(Bron bron) {
        this.bron = bron;
    }

    public String getRodzaj() {
        return rodzaj;
    }

    public void setRodzaj(String rodzaj) {
        this.rodzaj = rodzaj;
    }

    public String wyswietlPostac() {
        return ("(" + getRodzaj() + ") " + getImie() + ", bron: " + getBron().getRodzaj());
    }
}
