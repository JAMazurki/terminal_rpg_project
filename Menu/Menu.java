package Lab07.Menu;

import Lab07.PostacStrategia.Postac;

import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.Scanner;

public class Menu {

    public static void main(String[] args) {
        ArrayList<Postac> katalogPostaci = new ArrayList<>();
        StworzPostac tworzenie = new StworzPostac();
        Gra gra = new Gra();
        KlasaPomocnicza pomocnicza = new KlasaPomocnicza();
        Scanner scanner = new Scanner(System.in);
        String wybor;
        do {
            System.out.print("**********************************\n" + "            MENU GRY\n" + "**********************************\n");
            System.out.println("1 Stworz postac");
            System.out.println("2 Katalog postaci");
            System.out.println("3 Graj");
            System.out.println("4 Zakoncz gre");
            System.out.println("**********************************");
            wybor = scanner.next();

            switch (wybor) {
                case "1" -> katalogPostaci.add(tworzenie.stworzPostac());
                case "2" -> {
                    System.out.println("Stworzone postaci:");
                    KlasaPomocnicza.wyswietlPostaci(katalogPostaci);
                }
                case "3" -> {
                    if (!katalogPostaci.isEmpty()) {
                        System.out.println("Wybierz utworzoną postać");
                        KlasaPomocnicza.wyswietlPostaci(katalogPostaci);
                        try {
                            int numer = Integer.parseInt(scanner.next());
                            katalogPostaci.get(numer);
                            gra.graj(katalogPostaci.get(numer));
                        } catch (InputMismatchException | IndexOutOfBoundsException | NumberFormatException e) {
                            pomocnicza.komunikatBlad();
                        }
                    } else System.out.println("Aby zagrac musisz stworzyc postac");
                }
                case "4" -> System.out.println("Do zobaczenia!");
                default -> pomocnicza.komunikatBlad();
            }
        }
        while (!wybor.equals("4"));
    }
}


