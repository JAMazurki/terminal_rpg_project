package Lab07.Menu;

import Lab07.PostacStrategia.Postac;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

public class Gra {
    Scanner scanner = new Scanner(System.in);
    KlasaPomocnicza pomocnicza = new KlasaPomocnicza();
    ArrayList<String> dostepneRuchy = new ArrayList<>(Arrays.asList("Pokaz bron", "Eskploruj", "Wyjdz"));

    public void graj(Postac postac) {
        int wybor = 0;
        do {
            System.out.println("**********************************");
            System.out.println("Wybierz ruch: ");
            KlasaPomocnicza.wyswietlListe(dostepneRuchy);
            try {
                wybor = Integer.parseInt(scanner.next());
                switch (wybor) {
                    case 1 -> postac.getBron().pokazBron();
                    case 2 -> postac.eksploruj();
                    case 3 -> System.out.println("Konczysz tak szybko? :( ");
                    default -> System.out.println("Dotarles do miejsca gdzie diabel mowi dobranoc...Zawroc!");
                }
            } catch (NumberFormatException e) {
                pomocnicza.komunikatBlad();
            }
        }
        while (wybor - 1 != dostepneRuchy.indexOf("Wyjdz"));
    }
}
