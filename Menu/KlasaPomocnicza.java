package Lab07.Menu;

import Lab07.PostacStrategia.Postac;

import java.util.ArrayList;

public class KlasaPomocnicza {
    public static void wyswietlListe(ArrayList<String> lista) {
        if (lista.isEmpty()) System.out.println("Brak postaci do wyswietlenia :(");
        for (int i = 1; i < lista.size() + 1; i++) {
            System.out.println(i + " " + lista.get(i - 1));
        }
    }

    public static void wyswietlPostaci(ArrayList<Postac> listaPostaci) {
        int indeks = 0;
        if (listaPostaci.isEmpty()) System.out.println("Brak postaci do wyswietlenia :(");
        for (Postac postac : listaPostaci) {
            System.out.println((indeks++) + " " + postac.wyswietlPostac());
        }
    }

    public void komunikatBlad() {
        System.out.println("Wprowadzone dane sa nieprawidlowe");
    }
}
