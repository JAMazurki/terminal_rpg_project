package Lab07.Menu;

import Lab07.BronStrategia.*;
import Lab07.PostacStrategia.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.InputMismatchException;
import java.util.Scanner;

public class StworzPostac {
    KlasaPomocnicza pomocnicza = new KlasaPomocnicza();
    Scanner scanner = new Scanner(System.in);
    Scanner scanner2 = new Scanner(System.in);
    ArrayList<String> listaPostaci = new ArrayList<>(Arrays.asList("Chlop", "Czarodziejka", "Elf", "Krasnolud", "Krol", "Kucharka", "Marines", "Rycerz", "Smok", "Troll"));
    ArrayList<String> listaBroni = new ArrayList<>(Arrays.asList("Kamien", "Karabin", "Luk", "Miecz", "Patelnia", "Strumien ognia", "Topor", "Widly"));

    public Postac stworzPostac() {
        Postac postac = stworzNowaPostac();
        Bron bron = wybierzBron();
        postac.setBron(bron);
        System.out.println("**********************************");
        System.out.println("Wybierz imie dla swojego wojownika");
        postac.setImie(scanner2.nextLine());
        System.out.println("Postac zostala dodana do katalogu");
        return postac;
    }

    public Postac stworzNowaPostac() {
        System.out.println("**********************************");
        System.out.println("Wybierz postac");
        KlasaPomocnicza.wyswietlListe(listaPostaci);
        try {
            int rodzaj = Integer.parseInt(scanner.next());
            return switch (rodzaj) {
                case 1 -> new Chlop();
                case 2 -> new Czarodziejka();
                case 3 -> new Elf();
                case 4 -> new Krasnolud();
                case 5 -> new Krol();
                case 6 -> new Kucharka();
                case 7 -> new Marines();
                case 8 -> new Rycerz();
                case 9 -> new Smok();
                case 10 -> new Troll();
                default -> new Duch();
            };
        } catch (InputMismatchException | NumberFormatException e) {
            pomocnicza.komunikatBlad();
            return new Duch();
        }
    }

    public Bron wybierzBron() {
        System.out.println("**********************************");
        System.out.println("Wybierz bron, ktora chcesz uzywac");
        KlasaPomocnicza.wyswietlListe(listaBroni);
        try {
            int rodzaj = Integer.parseInt(scanner.next());
            return switch (rodzaj) {
                case 1 -> new Kamien();
                case 2 -> new Karabin();
                case 3 -> new Luk();
                case 4 -> new Miecz();
                case 5 -> new Patelnia();
                case 6 -> new StrumienOgnia();
                case 7 -> new Topor();
                case 8 -> new Widly();
                default -> new Bezbronny();
            };
        } catch (InputMismatchException | NumberFormatException e) {
            pomocnicza.komunikatBlad();
            return new Bezbronny();
        }
    }
}
