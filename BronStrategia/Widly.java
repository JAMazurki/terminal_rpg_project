package Lab07.BronStrategia;

public class Widly extends Bron {

    public Widly() {
        rodzaj = "widly";
    }

    @Override
    public void pokazBron() {
        System.out.println("Twoja bron: widly");
        System.out.println("'Niektorzy ludzie stoja w miejscu jak widly w gnoju.'");
    }
}
