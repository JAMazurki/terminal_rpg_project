package Lab07.BronStrategia;

public class Patelnia extends Bron {

    public Patelnia() {
        rodzaj = "patelnia";
    }

    @Override
    public void pokazBron() {
        System.out.println("Twoja bron: patelnia");
        System.out.println("'Kogut, ktory pieje najgłosniej, pierwszy skonczy na patelni.'");
    }
}
