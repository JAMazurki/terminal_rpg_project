package Lab07.BronStrategia;

public class Bezbronny extends Bron {
    public Bezbronny() {
        rodzaj = "brak broni";
        System.out.println("Jak mam walczyc bez broni?\nWybrana bron: brak");
    }

    @Override
    public void pokazBron() {
        System.out.println("Jestem bezbronny!!!");
    }
}
