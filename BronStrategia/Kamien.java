package Lab07.BronStrategia;

public class Kamien extends Bron {

    public Kamien() {
        rodzaj = "kamien";
    }

    @Override
    public void pokazBron() {
        System.out.println("Twoja bron: kamien");
        System.out.println("'Ten sam kamien słuzy i do kamieniowania, i do budowy.'");
    }
}
