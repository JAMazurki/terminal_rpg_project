package Lab07.BronStrategia;

public class StrumienOgnia extends Bron {

    public StrumienOgnia() {
        rodzaj = "strumien ognia";
    }

    @Override
    public void pokazBron() {
        System.out.println("Twoja bron: strumien ognia");
        System.out.println("'Morze, ogien i kobieta – to trzy nieszczescia.'");
    }
}
