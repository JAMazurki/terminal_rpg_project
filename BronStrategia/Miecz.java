package Lab07.BronStrategia;

public class Miecz extends Bron {

    public Miecz() {
        rodzaj = "miecz";
    }

    @Override
    public void pokazBron() {
        System.out.println("Twoja bron: miecz");
        System.out.println("'Strach tnie glebiej niż miecze.'");
    }
}
