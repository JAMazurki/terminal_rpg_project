package Lab07.BronStrategia;

public class Luk extends Bron {

    public Luk() {
        rodzaj = "luk";
    }

    @Override
    public void pokazBron() {
        System.out.println("Twoja bron: luk");
        System.out.println("'Luk jest napiety; nie wchodz w droge strzale.'");
    }
}
