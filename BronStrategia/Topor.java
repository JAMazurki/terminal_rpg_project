package Lab07.BronStrategia;

public class Topor extends Bron {

    public Topor() {
        rodzaj = "topor";
    }

    @Override
    public void pokazBron() {
        System.out.println("Twoja bron: topor");
        System.out.println("'Topor jest niczym bez reki, a reka niczym bez umyslu.'");
    }
}
