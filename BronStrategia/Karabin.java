package Lab07.BronStrategia;

public class Karabin extends Bron {

    public Karabin() {
        rodzaj = "karabin";
    }

    @Override
    public void pokazBron() {
        System.out.println("Twoja bron: karabin");
        System.out.println("'Pokoju [...] nie da sie zaprowadzic karabinami i wywlaszczeniami, wymaga to sprawiedliwosci i wzajemnego szacunku.'");
    }
}
